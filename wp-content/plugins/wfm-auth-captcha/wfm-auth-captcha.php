<?php /**
 * Plugin Name: Простая капча для формы авторизации
 * Description: Плагин добавляет простую проверку на человечность к форме авторизации
 * Plugin URI: http://m662449k.beget.tech/
 * Author: Тарас
 * Author URI: http://m662449k.beget.tech/
 * Version: 1.0.0
 */

// add_filter('login_errors', 'my_login_errors');

// function my_login_errors() {
//      return 'Ошибка авторизации';
// }

/*add_action('login_form', 'wfm_captcha_login');
add_action('wp_authenticate', 'wfm_authenticate', 10, 2);

function wfm_authenticate($username, $password) {
    if( isset($_POST['check']) && $_POST['check'] == 'check' ) {
       // wp_die( '<b>Ошибка</b>: вы не прошли проверку на человечность' );
    	add_filter('login_errors', 'my_login_errors');
    	$username = null;
    }

}

function my_login_errors() {
      return 'Вы не прошли проверку на человечность';
 }

function wfm_captcha_login() {
    echo '<p><label for="check"><input type="checkbox" name="check" id="check" value="check" checked> Снимите галочку </label></p>';
}
*/

add_action('login_form', 'wfm_captcha_login');
add_filter('authenticate', 'wp_auth_signon', 30, 3);

function wp_auth_signon($user, $username, $password) {
      if( isset($_POST['check']) && $_POST['check'] == 'check' ) {
          $user = new WP_Error('broke', '<b>Ошибка</b>: Вы бот?');
    }
    if(isset($user->errors['invalid_username']) || isset($user->errors['incorrect_password'])) {
         return new WP_Error('broke', '<b>Ошибка</b>: неверный логин/пароль');
    }
    return $user;
}

function wfm_captcha_login() {
    echo '<p><label for="check"><input type="checkbox" name="check" id="check" value="check" checked> Снимите галочку </label></p>';
}